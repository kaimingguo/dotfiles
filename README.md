# dotfiles

A configuration file - powered by [chezmoi](https://chezmoi.io).

<!--
[//]: <> vim: set tabstop=2 softtabstop=2 shiftwidth=2 textwidth=200 :
[//]: <> vim: set fileencoding=utf-8 filetype=markdown nofoldenable spell :
-->
